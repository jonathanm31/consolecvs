#ifndef NODE_H
#define NODE_H
#include "user.h"
#include <vector>
#include <ctime>
#include <string>
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include <fstream>
#include <cstdlib>
#include <ctime>
#include <sstream>

#define SSTR( x ) static_cast< std::ostringstream & >( \
        ( std::ostringstream() << std::dec << x ) ).str()


#define N 100000
static const char alphanum[] =
"abcdefghijklmnopqrstuvwxyz";
int stringLength = sizeof(alphanum) - 1;

using namespace std;

char genRandom()
{
    struct timespec ts;
        clock_gettime(CLOCK_MONOTONIC, &ts);

        /* using nano-seconds instead of seconds */
        srand((time_t)ts.tv_nsec);


    return alphanum[rand() % stringLength];
}
string genName(int color){
    string Str;
    for(unsigned int i = 0; i < ((3+color)%7 )+2; ++i)
    {
        Str += genRandom();

    }
    return Str;
}

class Node{
private:
    string m_file; // nombre del archivo etiqueta.hmm
    string m_etiqueta;
    int lines_delete, lines_update ;

public:

    User * m_user;
    char * created_at;
    char * modified_at;
    Node * m_prev;
    int state; // 1 activo, 0 inactivo, -1 eliminado
    int color; // 0 linea base, 1 branch
    vector<string>m_lineas;



    Node(){
        state = 1;
        m_prev = 0;
    }

    Node(int _color, vector<string> lineas){
        state = 1;
        color = _color;
        time_t now = time(0);
        created_at = modified_at = ctime(&now);
        lines_delete = lines_update = 0;
        m_prev = 0;
        m_etiqueta = genName(color);
        m_file = m_etiqueta +".cvs";
        m_lineas = lineas;
    }

    void set_users(User * u1){
        m_user = u1;
    }

    string get_archivo(){
        return m_file;
    }
    string get_etiqueta(){
        return m_etiqueta;
    }

    string print(){
        string ret;
        ret = "";
        if(state >= 0){
            if(state == 1)
            {ret += " shape=doublecircle ";}else{
             ret += " shape=circle ";
            }
            if(color > 0){
               ret += " color=indianred1";
            }else{
                ret += " color=darkturquoise ";
            }
        }else{
           ret += " shape=Mcircle color= \"#993399\" fillcolor=\"#f2e6ff\" ";
        }
        return this->m_etiqueta + "[label=\" "+ SSTR(color)+ " \n "+ this->m_etiqueta  +"\n " + m_user->name + " \" "+ ret + " ]\n";
    }

    void evaluar_archivo(char archivo[N]);


};

class Edge{
public:
    Node * childs[2];
    bool state ; // camino a un nodo eliminado = -1; camino  aun nodo optimo = 1
    string label;
    Edge(){
        childs[0] = childs[1] = 0;
        state = 0;
        label = "";

    }

    Edge(Node * a, Node * b){
        childs[0] = a;
        childs[1] = b;
        state = 1;
    }
    string print(){
        string ret;
        ret = "";
        if(state == 0)ret += "[color= \"#993399\" arrowhead=empty ]";
        return childs[0]->get_etiqueta() + " -> " + childs[1]->get_etiqueta() + label + ret +" \n";
    }

};


void Node::evaluar_archivo(char archivo[N]){

    if(m_prev){
        ifstream infile;
        infile.open("input.txt");

        //check error

        if(infile.fail()){
            cerr << "No se pudo abrir" <<endl;
            exit(1);
        }

        string linea;
        vector<string> lineas;

        int i = 0;
        //read file to end
        while(getline (infile,linea)){
            lineas.push_back(linea);
        }
        infile.close();

        for(int j = 0 ;  j < lineas.size() ; j++){
            cout << lineas[j] << endl;
        }

    }
}

vector<int> comparar(vector<string> doc1, vector<string> doc2){


}

#endif // NODE_H
