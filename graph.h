#ifndef GRAPH_H
#define GRAPH_H
#include "node.h"
#include "user.h"
#include <vector>
#include <fstream>
#include <iostream>     // std::cout, std::ostream, std::ios
#include <stdio.h>      /* printf */
#include <stdlib.h>
#include <utility>


using namespace std;

class item{
   public:
    vector<pair<Node*,int > > nodos;
    vector<pair<Edge*,int > > edges;
    item * _prev;
    item * _sig;

    int _time;
    int state;
    item(int time){
        _prev = _sig = 0;
        _time = time;
        state = 1;
    }
    ~item(){

        delete _prev;
        delete _sig;
    }
    void addNode(vector<pair<Node*,int> > _nodos){
        nodos = _nodos;
    }
    void addEdge(vector<pair<Edge*,int> > _edges){
        edges = _edges;
    }


};

class lista{
public:
    item * head;
    int t;

    lista(){
        t = 0;
        head = 0;
    }

    void insert(vector<Node *> nodos,vector<Edge *> edges ){
        item *temp = head;
        item * prev;

        vector<pair<Node*,int> > ndos;
        vector<pair<Edge*,int> > eges;

        for(int i = 0; i < nodos.size(); i++){
            pair<Node* , int> a;
            a.first= nodos[i];
            a.second = nodos[i]->state;
            ndos.push_back(a);
        }

        for(int i = 0; i < edges.size(); i++){
            pair<Edge*, int> ed;
            ed.first = edges[i];
            ed.second = edges[i]->state;
            eges.push_back(ed);
        }
        if(head == 0){
            temp = new item(t);
            prev = 0;
            head = temp;

        }else{
            prev = temp;
            while(temp){
                prev = temp;
                temp = temp->_sig;
            }
            temp = new item(t);
            temp->_prev = prev;
            prev->_sig = temp;
            prev->state = 0;
        }
        temp->addNode(ndos);
        temp->addEdge(eges);
        t++;
    }
    void mostrar(int _t){
        item *temp = head;
        while(temp){
            temp = temp->_sig;
            cout << "tiempo : " << temp->_time << endl;
        }
    }
};


class Graph{
public:
 Node * m_root;
 int color_idx;
 vector<Node *> nodes;
 vector<Edge *> edges;
 lista persistencia;
 Graph(){
     m_root = 0;
     color_idx = 0;
 }

 Node * insertion(int color,vector<string> lineas,User *user);
 Node * new_branch(int color, vector<string> lineas, User * user);
 Node * busqueda(string etiqueta);
 vector<Node *> Path(string etiqueta);
 Node * busqueda(Node * temp);
 vector<Node *> Path(Node * temp);

 void eliminar(string etiqueta);

 void graph_dot();

 Node * Merge(Node * a, Node * b,int i, int j);
 Node * Merge(Node * a, Node * b);
 void MergeColor(int colorA, int colorB);

 void Restaurar(string etiqueta);
 bool has_edge(Node * a){
     for(int i = 0; i < edges.size(); i++){
         if(edges.at(i)->childs[0] == a)return true;
     }
     return false;
 }

};

Node * Graph::insertion(int color,vector<string> lineas,User * user){
    Node * temp = 0;
    for(int j =0 ; j < this->nodes.size(); j++){
        if(nodes[j]->color == color && nodes[j]->state > 0 ){
            temp = nodes[j];
            break;
        }
    }

    vector<Node*> Pnodes;
    vector<Edge*> Pedges;


    if(temp){

        if(temp->m_user != user){
            //DIFERENTES USUARIOS

            //CREAMOS UNA BRANCH NUEVA

            Node * aux = new_branch(color,lineas,user);
            aux->state = 0;


            //Nodo base
            Node * node = new Node(color,lineas);
            node->m_user = user;
            Edge * newEd = new Edge(aux,node);
            newEd->label = "[ label= \" auto merge  \"  color= \"#ffcc00\"fontcolor=\" #cc3300 \"  ]";
            temp->state = 0;
            Pnodes.push_back(temp);


            edges.push_back(newEd);
            nodes.push_back(node);

            //MERGE

            Edge * ed = new Edge(temp,node);
            node->m_prev = temp;
            ed->label = "[ label= \" auto merge  \"  color= \"#ffcc00\" fontcolor=\" #cc3300 \" ]";
            edges.push_back(ed);

            /**********PERSISTENCIA ******/

            Pnodes.push_back(node);
            Pnodes.push_back(aux);
            Pedges.push_back(newEd);
            Pedges.push_back(ed);

            persistencia.insert(Pnodes,Pedges);



            return node;


        }else{
            Node * node = new Node(color,lineas);
            node->m_prev = temp;
            node->m_user = user;
            Pnodes.push_back(node);

            Edge * newEd = new Edge(temp,node);
            Pedges.push_back(newEd);

            temp->state = 0;
            Pnodes.push_back(temp);

            edges.push_back(newEd);
            nodes.push_back(node);

            /********PERSISTENCIA *******/



            persistencia.insert(Pnodes,Pedges);
            return node;

        }


    }else{
        Node * node = new Node(color,lineas);
        nodes.push_back(node);
        node->m_user = user;

        /********PERSISTENCIA *******/


        Pnodes.push_back(node);

        persistencia.insert(Pnodes,Pedges);

        return node;
    }
}

Node * Graph::new_branch(int color, vector<string> lineas, User *user){
    Node * temp = 0;
    vector<Node*> Pnodes;
    vector<Edge*> Pedges;

    for(int j =0 ; j < this->nodes.size(); j++){
        if(nodes[j]->color == color && nodes[j]->state > 0 ){
            temp = nodes[j];
            break;
        }
    }

    if(!temp)return 0;
    Pnodes.push_back(temp);


    color_idx ++;
    Node * node = new Node(color_idx,lineas);
    node->m_user = user;
    node->m_prev = temp;

    Pnodes.push_back(node);
    Pnodes.push_back(temp);

    Edge * newEdg = new Edge(temp,node);
    Pedges.push_back(newEdg);

    edges.push_back(newEdg);
    nodes.push_back(node);



    //PERSISTENCIA//
    persistencia.insert(Pnodes,Pedges);

    //FIN PERSISTENCIA//

    return node;

}

Node * Graph::Merge(Node *a, Node *b,int i, int j){
    //MERGE ARCHIVO

    //NUEVO NODO
    Node * temp= new Node(a->color,b->m_lineas);
    temp->m_prev = a;
    temp->m_user = b->m_user;
    nodes.push_back(temp);

    //CREAR ARISTAS

    Edge * ed1 = new Edge(a,temp);
    Edge * ed2 = new Edge(b,temp);
    edges.push_back(ed1);
    edges.push_back(ed2);

    ed1->label = "[ label= \" merge  \"  color= \"darkturquoise;0.25:indianred1\" ]";
    ed2->label = "[ label= \" merge  \"  color= \"indianred1;0.25:darkturquoise\" ]";



    //actualizar lista de activos
    a->state = 0;
    b->state = 0;


    /********PERSISTENCIA *******/

    vector<Node*> Pnodes;
    vector<Edge*> Pedges;
    Pnodes.push_back(temp);
    Pnodes.push_back(a);
    Pnodes.push_back(b);
    Pedges.push_back(ed1);
    Pedges.push_back(ed2);

    persistencia.insert(Pnodes,Pedges);


    return temp;
}

Node * Graph::Merge(Node *a, Node *b){
    //MERGE ARCHIVO

    //NUEVO NODO
    Node * temp= new Node(a->color,b->m_lineas);
    temp->m_prev = a;
    temp->m_user = b->m_user;
    nodes.push_back(temp);

    //CREAR ARISTAS

    Edge * ed1 = new Edge(a,temp);
    Edge * ed2 = new Edge(b,temp);
    edges.push_back(ed1);
    edges.push_back(ed2);

    ed1->label = "[ label= \" merge  \"  color= \"darkturquoise;0.25:indianred1\" ]";
    ed2->label = "[ label= \" merge  \"  color= \"indianred1;0.25:darkturquoise\" ]";



    //actualizar lista de activos
    a->state = 0;
    b->state = 0;

    int j = 0;


    return temp;
}

void Graph::MergeColor(int colorA, int colorB){

    vector<Node*> Pnodes;
    vector<Edge*> Pedges;


    //MERGE ARCHIVO

    //actualizar lista de activos

    Node * a1 = 0;
    Node * a2 = 0;

    for(int j =0 ; j < this->nodes.size(); j++){
        if(nodes[j]->color == colorA && nodes[j]->state > 0 ){
            a1 = nodes[j];
            break;
        }
    }

    for(int j =0 ; j < this->nodes.size(); j++){
        if(nodes[j]->color == colorB && nodes[j]->state > 0 ){
            a2 = nodes[j];
            break;
        }
    }

    a1->state = 0;
    a2->state = 0;

    //NUEVO NODO
    Node * temp= new Node(a1->color,a2->m_lineas);
    temp->m_prev = a1;
    temp->m_user = a2->m_user;
    nodes.push_back(temp);

    /**agregar a lista de persistencias **/
    Pnodes.push_back(temp);


    //CREAR ARISTAS

    Edge * ed1 = new Edge(a1,temp);
    Edge * ed2 = new Edge(a2,temp);
    edges.push_back(ed1);
    edges.push_back(ed2);
    Pedges.push_back(ed1);
    Pedges.push_back(ed2);


    ed1->label = "[ label= \" merge  \"  color= \"darkturquoise;0.25:indianred1\" fontcolor=\" #cc3300 \" ]";
    ed2->label = "[ label= \" merge  \"  color= \"indianred1;0.25:darkturquoise\" fontcolor=\" #cc3300 \"]";

    /**agregar a lista de persistencias **/
    Pnodes.push_back(a1);
    Pnodes.push_back(a2);


    /********PERSISTENCIA *******/

    persistencia.insert(Pnodes,Pedges);

}

void Graph::Restaurar(string etiqueta){
    int i;
    for(i=0 ; i < nodes.size(); i ++)
        if(nodes[i]->get_etiqueta().compare(etiqueta) == 0)break;



    if(i == nodes.size())return;

    Node *rest = nodes[i];
    vector<Node *> activos;

    for(int j= 0; j < edges.size();j++)
        if(edges[j]->childs[1] == rest && edges[j]->state == 0)
        {
            edges[j]->state = 1;
        }


    //CAMBIAR TODOS LOS NODOS HIJOS DEL NODO ELIMINADO
    vector<Node * > paths;

    paths.push_back(rest);

    while(!paths.empty()){
        Node * temp = paths.back();
        paths.pop_back();
        if(temp->state == -1){
            temp->state = 0;
            if(!has_edge(temp))activos.push_back(temp);
            for(int j =0; j < edges.size(); j++){
                if(edges[j]->childs[0] == temp ){
                    edges[j]->state = 1;
                    paths.push_back(edges[j]->childs[1]);
                }
            }
        }

    }
    if(rest->m_prev->state == 1){
        rest->m_prev->state = 0;
        rest->state = 1;
        activos.push_back(rest);
    }


    while(!activos.empty()){
        Node *act = activos.back();
        act->state = 1;
        activos.pop_back();

    }


}

void Graph::eliminar(string etiqueta){
    int i;
    for(i=0 ; i < nodes.size(); i ++)
        if(nodes[i]->get_etiqueta().compare(etiqueta) == 0)break;
    if(i == nodes.size())return;
    int old_state = nodes[i]->state;
   // nodes[i]->state = -1;
    bool esta = false;



    for(int k = 0; k < edges.size(); k++){
        if(edges[k]->childs[1] == nodes[i] ){
            edges[k]->state = 0;
            //ACTIVAR NODOS ANTERIORES SI EL NODO ERA ACTIVO
            if(esta){
                edges[k]->childs[0]->state = 1;
            }


        }
    }

    //CAMBIAR TODOS LOS NODOS HIJOS DEL NODO ELIMINADO
    vector<Node * > paths;
    vector<Node *> activos;
    paths.push_back(nodes[i]);
    bool ramaBase = false;

    while(!paths.empty()){
        Node * temp = paths.back();
        paths.pop_back();
        if(temp->state != -1){
            temp->state = -1;
            for(int j =0; j < edges.size(); j++){
                if(edges[j]->childs[0] == temp ){
                    edges[j]->state = 0;
                    paths.push_back(edges[j]->childs[1]);
                    if(edges[j]->childs[1]->state == 1){
                        activos.push_back(edges[j]->childs[1]);
                    }
                }
            }
        }

    }

    while(!activos.empty()){
        Node * temp = activos.back();
        activos.pop_back();
        int color = temp->color;

        while(temp->m_prev && temp->m_prev->state == -1 )
        {
            temp = temp->m_prev;
        }
        if(temp->m_prev->state != -1 && temp->m_prev->color == color )
        {
            temp->m_prev->state = 1;
        }


    }


}

void Graph::graph_dot(){

      string ret;
      ret = "";
      ret += "digraph BST{";
      ret += "node [shape=plaintext]";
      ret += "subgraph cluster_01 { \n";
      ret +=  "label = \"Leyenda\"; \n";
      ret +=  "key [label=<<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" cellborder=\"0\"> \n ";
      ret +=  "       <tr><td align=\"right\" port=\"i1\">Camino Muerto</td></tr> \n ";
      ret +=  "        <tr><td align=\"right\" port=\"i2\">Commit</td></tr>\ \n";
      ret +=  "        <tr><td align=\"right\" port=\"i3\">Merge</td></tr>\ \n";
      ret +=  "        <tr><td align=\"right\" port=\"i4\">Commit != usuario</td></tr>\ \n";
      ret +=  "        </table>>] \n ";
      ret +=  "key2 [label=<<table border=\"0\" cellpadding=\"2\" cellspacing=\"0\" cellborder=\"0\"> \n ";
      ret +=  "       <tr><td port=\"i1\">&nbsp;</td></tr> \n";
      ret +=  "       <tr><td port=\"i2\">&nbsp;</td></tr> \n";
      ret +=  "       <tr><td port=\"i3\">&nbsp;</td></tr> \n";
      ret +=  "       <tr><td port=\"i4\">&nbsp;</td></tr> \n";
      ret +=  "        </table>>] \n ";
      ret +=  "     key:i1:e -> key2:i1:w [color= \"#993399\" arrowhead=empty  ] \n";
      ret +=  "      key:i2:e -> key2:i2:w [color=\"#000000\"] \n";
      ret +=  "      key:i3:e -> key2:i3:w [color= \"darkturquoise;0.25:indianred1\"] \n";
      ret +=  "      key:i4:e -> key2:i4:w [color= \"#ffcc00\"] \n";
      ret +=  "ND[label=\" Nodo Eliminado \"] \n";
      ret +=  "ND1[label=\" \", shape=Mcircle, color= \"#993399\", fillcolor=\"#f2e6ff\" ] \n";
      ret += "ND -> ND1[arrowhead=none style=dotted ] \n";
      ret +=  "NB[label=\" Nodo Base \"] \n";
      ret +=  "NB1[label=\" \", shape=circle, color= darkturquoise ] \n";
      ret += "NB -> NB1[arrowhead=none style=dotted] \n";
      ret +=  "NBA[label=\" Nodo Base Activo \"] \n";
      ret +=  "NBA1[label=\" \", shape=doublecircle, color= darkturquoise ] \n";
      ret += "NBA -> NBA1[arrowhead=none style=dotted] \n";

      ret +=  "NBR[label=\" Nodo Rama \"] \n";
      ret +=  "NBR1[label=\" \", shape=circle, color= indianred1 ] \n";
      ret += "NBR -> NBR1[arrowhead=none style=dotted] \n";
      ret +=  "NBRA[label=\" Nodo Rama Activo \"] \n";
      ret +=  "NBRA1[label=\" \", shape=doublecircle, color= indianred1 ] \n";
      ret += "NBRA -> NBRA1[arrowhead=none style=dotted] \n";
      ret += "     } \n";
              ret += '\n';
              ret += "node [style=filled];";
              ret += '\n';



      for(int i = 0 ; i < nodes.size(); i++)
          ret += nodes.at(i)->print();


      for(int j = 0; j < edges.size(); j++)
          ret += edges[j]->print();
      ret += "\n";

      Node * NB ;

      for(int j =0 ; j < this->nodes.size(); j++){
          if(nodes[j]->color == 0 && nodes[j]->state > 0 ){
              NB = nodes[j];
              break;
          }
      }


      ret += "subgraph cluster_02 { \npencolor=transparent\n";

      while(NB->m_prev)
      {
          ret += NB->get_etiqueta() + " -> ";
          NB = NB->m_prev;
      }
      ret += NB->get_etiqueta() + " [style=invis] \n ";
      ret +=  "}";

      ret += '\n';
      ret +=  "}";


      string filename_("graph");
      string extDot= filename_+".dot";
      string extPNG = filename_+".dot";
      string genDot = "dot -Grankdir=LR   -Tpng "+filename_+".dot > "+filename_+".png";
     // string seePNG = "eog "+filename_+".png";
      ofstream out(extDot.c_str());
      //in the function tree.string_to_export_in_dot is the magic
      out << ret;
      out.close();
            //its time to generate the png
      system(genDot.c_str());
            //this is optional, if you dont like, only remove
      //system(seePNG.c_str());
}

Node * Graph::busqueda(string etiqueta){
    for(int i = 0; i < nodes.size(); i++)
        if(nodes.at(i)->get_etiqueta() == etiqueta)
            return nodes.at(i);

    return 0;
}
vector<Node *> Graph::Path(string etiqueta){
    Node * temp = busqueda(etiqueta);
    vector<Node * > path;
    path.push_back(temp);
    while(temp->m_prev){
        path.push_back(temp->m_prev);
        temp  = temp->m_prev;
    }
    return path;
}

Node * Graph::busqueda(Node * temp){
    for(int i = 0; i < nodes.size(); i++)
        if(nodes.at(i) == temp)
            return nodes.at(i);

    return 0;
}
vector<Node *> Graph::Path(Node * temp){
    Node * aux = busqueda(temp);
    vector<Node * > path;
    path.push_back(aux);
    while(aux->m_prev){
        path.push_back(aux->m_prev);
        aux  = aux->m_prev;
    }
    return path;
}

class Persistence{
public:
    lista * Tiempos;
    Graph * grafo;

    Persistence(lista * lt, Graph * g){
        Tiempos = lt;
        grafo = g;
    }

    void Restaurar(int t){
        item *temp = Tiempos->head;
        item *now = Tiempos->head;
        while(now->state < 1){
            now = now->_sig;
        }
        now->state = 0;

        int time = t -1;
        while(temp->_time < time){
            for(int i =0 ; i < temp->nodos.size(); i++){
                temp->nodos.at(i).first->state = temp->nodos.at(i).second;
            }
            for(int i =0 ; i < temp->edges.size(); i++){
                temp->edges.at(i).first->state = temp->edges.at(i).second;
            }
            temp = temp->_sig;
        }
        temp->state = 1;

        /*for(int j = 0; j < temp->nodos.size(); j++){
            vector<Node * > Lista;
            Node * aux = temp->nodos.at(j).first;

            for(int i= 0; i < grafo->edges.size(); i++ ){
                if(grafo->edges.at(i)->childs[0] == aux){
                    Lista.push_back(grafo->edges.at(i)->childs[1]);
                }
            }

            for(int i=0; i < Lista.size(); i++){
                grafo->eliminar(Lista.at(i)->get_etiqueta());
            }


        }*/
        item *aux = temp->_sig;
        for(int i = temp->_time; i < now->_time; i++){
            for(int j = 0 ; j < aux->nodos.size(); j++){
                grafo->eliminar(aux->nodos.at(j).first->get_etiqueta());
            }
            aux = aux->_sig;

        }


    }

};

#endif // GRAPH_H
