QT += core
QT -= gui

CONFIG += c++11

TARGET = ConsoleCVs
CONFIG += console
CONFIG += c++11
CONFIG -= app_bundle
QMAKE_CXXFLAGS += -std=c++11

TEMPLATE = app

SOURCES += main.cpp

HEADERS += \
    node.h \
    user.h \
    graph.h \
    doc.h \
    persistence.h

DISTFILES += \
    ../build-ConsoleCVs-Desktop-Debug/input.txt \
    ../build-ConsoleCVs-Desktop-Debug/graph.dot
