#ifndef USER_H
#define USER_H

#include <string>
#include <iostream>
#include <ctime>

using namespace std;

class User{
public:
    string name;
    bool status;
    int ID;
    char * last_session;

    User(string _name, int _id){
        status = true;
        name = _name;
        ID = _id;
        time_t now = time(0);
        last_session = ctime(&now);
    }
    void session(){
        time_t now = time(0);
        last_session = ctime(&now);
    }
};

#endif // USER_H
