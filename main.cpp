#include <QCoreApplication>
#include "graph.h"
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include <unistd.h>
#include <numeric>
#include <chrono>

using namespace std;
#define N 100000

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    auto start = chrono::system_clock::now();

    ifstream infile;
    infile.open("input.txt");

    //check error

    if(infile.fail()){
        cerr << "No se pudo abrir" <<endl;
        exit(1);
    }

    string linea;
    vector<string> lineas;

    int i = 0;
    //read file to end
    while(getline (infile,linea)){
        lineas.push_back(linea);
    }
    infile.close();

    int mem[lineas.size()];

    for(int j = 0 ;  j < lineas.size() ; j++){
        for(int k = 1; k < lineas.size(); k++){
            if(!lineas[j].compare(lineas[k]) && j != k ){
                //cout<< j << " es igual a: "<< k << endl;
            }
        }
    }
   User * user = new User("harry", 1);
   User * user1 = new User("Vanne", 2);
   Graph grafo;
   Persistence P(&(grafo.persistencia),&grafo);

 /*  grafo.insertion(0,lineas,user); //0
   grafo.insertion(0,lineas,user); //1

   grafo.insertion(0,lineas,user); //2
   grafo.insertion(0,lineas,user); //3
   grafo.insertion(0,lineas,user); //4
   grafo.insertion(0,lineas,user); //5
   grafo.insertion(0,lineas,user); //6
   grafo.new_branch(0,lineas,user); //7
   grafo.new_branch(1,lineas,user); //8
   grafo.insertion(1,lineas,user); //9
   grafo.insertion(2,lineas,user); //10
   grafo.new_branch(2,lineas,user); //11
   grafo.insertion(3,lineas,user); //12
   grafo.MergeColor(1,2);           //12
   grafo.MergeColor(1,3);           //13
   P.Restaurar(8);
   grafo.insertion(1,lineas,user);
   P.Restaurar(3);

   //___________

   grafo.new_branch(0,lineas,user); //11
   grafo.insertion(4,lineas,user); //12
   grafo.MergeColor(0,4);           //13
   grafo.insertion(0,lineas,user); //14
   P.Restaurar(8);
   grafo.insertion(1,lineas,user); //15
   grafo.MergeColor(0,1);
   for(int j = 0; j < 50000; j++){
       grafo.insertion(0,lineas,user);
   }*/

     grafo.insertion(0,lineas,user); //0
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //1
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.new_branch(0,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(1,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.new_branch(1,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(2,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(2,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.MergeColor(1,2);

     grafo.new_branch(1,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);


     grafo.new_branch(1,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.new_branch(1,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.new_branch(1,lineas,user); //2
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(3,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.MergeColor(0,1);
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);


     grafo.insertion(3,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(3,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(4,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(4,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(4,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(4,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(5,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(5,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.MergeColor(0,4); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(5,lineas,user); //3
     grafo.graph_dot();
     sleep(1);


     grafo.insertion(5,lineas,user); //3
     grafo.graph_dot();
     sleep(1);


     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(6,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(6,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(6,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(6,lineas,user); //3
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user1); //3
     grafo.graph_dot();
     sleep(10);

     P.Restaurar(25);
     grafo.graph_dot();
     sleep(1);

     grafo.insertion(0,lineas,user1); //3
     grafo.graph_dot();












   for(int i = 0 ; i < grafo.nodes.size(); i++){
    cout << "etiq: "<< grafo.nodes.at(i)->get_etiqueta()  <<" color: " << grafo.nodes.at(i)->color << " estado: " << grafo.nodes.at(i)->state << " user: " << grafo.nodes.at(i)->m_user->name << endl;

   }
  // grafo.graph_dot();
   cout << grafo.persistencia.t << endl;

   auto end = std::chrono::system_clock::now();
   std::chrono::duration<double> diff = end-start;
   std::cout << "Time " << " ints : " << diff.count() << " s\n";



  /* int c = 1;
   int u = 0;
   while(c > 0){
       cout << "-1\tCommit \n-2\tCommit New branch \n-3\tMostrar \n-4\tMerge \n-2\tEliminar\n-6\tRestaurar \n-0\tSalir"<< endl;
       cin>> c;
       cout << "1\tharry\t\t2\tvane "<< endl;
       cin>> u;
       int r = 0;
       if(c == 1){
           cout << "rama? " << endl;
           cin >> r;
           if(u==1){
               grafo.insertion(r,lineas,user);
           }else{
               grafo.insertion(r,lineas,user1);

           }
           grafo.graph_dot();


       }else if(c == 2){
           cout << "rama? " << endl;
           cin >> r;
           if(u==1){
               grafo.new_branch(r,lineas,user);
           }else{
               grafo.new_branch(r,lineas,user1);

           }
           grafo.graph_dot();


       }else if(c == 3){
           for(int i = 0 ; i < grafo.nodes.size(); i++){
            cout << "etiq: "<< grafo.nodes.at(i)->get_etiqueta()  <<" color: " << grafo.nodes.at(i)->color << " estado: " << grafo.nodes.at(i)->state << " user: " << grafo.nodes.at(i)->m_user->name << endl;

           }
           grafo.graph_dot();
       }else if(c == 4){
           int c1, c2;
           cout<< "ingresar ramas" <<endl;
           cin>>c1;
           cin>>c2;
           grafo.MergeColor(c1,c2);
           grafo.graph_dot();


       }else if(c == 2){
           string et;
           cout << "\tEtiqueta?"<< endl;
           cin>>et;
           grafo.eliminar(et);
           grafo.graph_dot();

       }
       else if(c == 6){
                  string et;
                  cout << "\tEtiqueta?"<< endl;
                  cin>>et;
                  grafo.Restaurar(et);
                  grafo.graph_dot();

              }
       else if(c == 7){
                  int t;
                  cout << "\tiempo?"<< endl;
                  cin>>t;
                  grafo.persistencia.mostrar(t);
              }
   }*/

    return a.exec();
}
